import { Injectable } from '@angular/core';
import { elementAt } from 'rxjs';
import { IDatos } from '../interfaces/citas';

@Injectable({
  providedIn: 'root'
})
export class CitaService {
  LISTA_CITAS: IDatos[] = [

    {nombre:'Maritza',apellido:'Abarca',correo:'maeillanes@hotmail.com',celular:67505071,fecha:'14/02/2022',hora:'14:00',descripcion:'hola'},
    {nombre:'Osvaldo',apellido:'Abarca',correo:'osabarca@hotmail.com',celular:67505071,fecha:'14/02/2028',hora:'14:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo'},
    {nombre:'Elizabeth',apellido:'Armstrong',correo:'elizabetharmstrong39@gmail.com',celular:89897071,fecha:'14/02/2028',hora:'15:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo'},
    {nombre:'Maria Angelica',apellido:'Berguez',correo:'angelicabergez@gmail.com',celular:67505071,fecha:'14/02/2022',hora:'14:00',descripcion:'hola'},
    {nombre:'Pablo',apellido:'Calderon',correo:'pablo.calderon.cadiz@gmail.com',celular:67505071,fecha:'14/02/2028',hora:'14:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo'},
    {nombre:'Lourdes',apellido:'Vela Diaz',correo:'velaLou@gmail.com',celular:89897071,fecha:'14/02/2028',hora:'15:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo' },
    {nombre:'Camila',apellido:'Muñoz',correo:'camilamo576@gmail.com',celular:67505071,fecha:'14/02/2022',hora:'14:00',descripcion:'hola'},
    {nombre:'Maria del Pilar',apellido:'Balderrama Flores',correo:'camilamo576@gmail.com',celular:67505071,fecha:'14/02/2028',hora:'14:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo'},
    {nombre:'Lourdes',apellido:'Vela Diaz',correo:'velaLou@gmail.com',celular:89897071,fecha:'14/02/2028',hora:'15:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo'}
  ];
  constructor() { }

  getCitas(){
    return this.LISTA_CITAS.slice();
  }

  agregarCita(cita:IDatos){
    this.LISTA_CITAS.push(cita);
  }

  eliminarCita(nombre:string){
    this.LISTA_CITAS=this.LISTA_CITAS.filter(data =>{
      return data.nombre !== nombre;
    })
  }

  modificarCita(names:IDatos){
    this.eliminarCita(names.nombre);
    this.agregarCita(names);
  }

  buscarCita(id:string):IDatos{
    return this.LISTA_CITAS.find(element => element.nombre === id) || {} as IDatos;
  }
}
